# -*- coding: utf-8 -*-
"""
Created on Sun Nov 29 12:41:54 2020

@author: ASUS
"""


import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.model_selection import cross_val_score



training = pd.read_excel("D:\KULIAH\Pemrosesan Citra\Projek\citra.xlsx", 
                             sheet_name='Data Training')
test = pd.read_excel("D:\KULIAH\Pemrosesan Citra\Projek\citra.xlsx", 
                             sheet_name='Data Test')

# data = pd.read_excel("D:\KULIAH\Pemrosesan Citra\Projek\Citra.xlsx", 
#                               sheet_name='Data Besar Sorted')


x_train = training.drop(["Level"], axis=1)
x_test = test.drop(["Level"], axis=1)

y_train = training["Level"]
y_test = test["Level"]


# X = data.drop(columns=['Level'])
# y = data["Level"].values


#x_train, x_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 123)
# scaler = StandardScaler()  
# scaler.fit(x_train)
# x_train = scaler.transform(x_train)  
# x_test = scaler.transform(x_test)

knn = KNeighborsClassifier(n_neighbors=1, metric='cityblock')
knn.fit(x_train, y_train)
y_pred = knn.predict(x_test)
cmat=confusion_matrix(y_test, y_pred)
#cv_scores = cross_val_score(knn, X, y, cv=2)
# print(knn.score(x_test, y_test))
#print(cv_scores)
print(y_pred)
print(cmat)
print (classification_report(y_test, y_pred))